<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('user')) {
            return $next($request);
        }

        Session::forget('user');

        return responseRedirect(0,'Please Login!', 'getLogin');

    }
}
