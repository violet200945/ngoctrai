<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use App\Model\GoogleAuth;

class LoginController extends Controller
{

    public function getLogin()
    {
        return view('Auth.Login');
    }

    public function postLogin(Request $request) {
        $request->validate([
            'email' => 'bail|required|exists:users,User_Email',
            'password' => 'bail|required',
            'g-recaptcha-response' => 'bail|required'
        ],[
            'g-recaptcha-response.required' =>'Capchar code required!'
        ]);

        $googleCapcharData = [
            'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
            'response' => $request->input('g-recaptcha-response')
        ];

        $response = postAPI("https://www.google.com/recaptcha/api/siteverify", $googleCapcharData);
        $responseData = json_decode((string)$response->getBody());
        if ($responseData->success != true) {
            return responseRedirect(0, 'ReCapchar incorrect!');
        }

        $user = User::where('User_Email', $request->email)->first();
        if (!Hash::check($request->password, $user->User_Password)) {
            return responseRedirect(0, "Password is incorrect!");
        }

        $auth = GoogleAuth::where('google2fa_User',$user->User_ID)->first();
        if($auth){
            Session::put('auth',$auth);
            $otp = true;
            return redirect()->route('getLogin')->with(['otp'=>$otp]);
        }
        writeLog($user->User_ID, 'Login', null, 'Login User ID: '.$user->User_ID);
        Session::put('user', $user);
        return responseRedirect(0, ERROR_CONTACT_AMDIN);

    }

    public function getLogout()
    {
        $user = session('user');
        $userTemp = session('user_temp');
        if ($userTemp) {
            Session::put('user', $userTemp);
            Session::forget('user_temp');
            writeLog($userTemp->User_ID, 'Login', null, "Admin: $userTemp->User_ID login from account: $user->User_ID" );
            return redirect()->route('system.getDashboard')->with(['flash_level' => 'success', 'flash_message' => 'Login successfully']);
        }
        Session::forget('user');
        writeLog($user->User_ID, 'Logout', null, 'Logout User ID: '.$user->User_ID);
        return redirect()->route('getIndex')->with(['flash_level' => 'success', 'flash_message' => 'Logout successfully']);
    }

    public function postLoginCheckOTP(Request $request){
        $auth = Session('auth');
        $google2fa = app('pragmarx.google2fa');
        $valid = $google2fa->verifyKey($auth->google2fa_Secret, $request->otp);
        if($valid){
            $user = User::find($auth->google2fa_User);
            Session::put('user', $user);
            return 1;
        }
        return 0;
    }
}
