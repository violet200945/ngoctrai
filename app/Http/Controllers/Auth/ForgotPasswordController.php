<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Support\Facades\DB;


class ForgotPasswordController extends Controller
{
    public function getForgotPassword()
    {
        return view('Auth.Forgot-Password');
    }

    public function postForgotPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users,User_Email'
        ]);

        $password = $this->generateRandomString(10);
        $user = User::where('User_Email', $request->email)->first();
        $user->update(['User_Password'=>bcrypt($password)]);
        $data = array('password' => $password);
        sendMail('Mail.Mail-Forgot-Password', $data, $request->email, 'Reset Password');
        writeLog($user->User_ID, 'Reset Password', null, "");
        return responseRedirect(1, 'Please check mail to get new password!', 'getLogin');

    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
