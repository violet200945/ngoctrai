<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    public function getRegister()
    {
        return view('Auth.Register');
    }

    public function postRegister(Request $request)
    {
        $request->validate([
            'email' => 'bail|required|unique:users,User_Email',
            'password' => 'bail|required|min:6|max:255',
            're_password' => 'bail|same:password',
            'sponser' => 'bail|required|exists:users,User_ID'
        ]);

        $sponserInfo = User::where('User_ID', $request->sponser)->first();
        $userID = $this->RandonIDUser();

        $userData['User_Email'] = $request->email;
        $userData['User_ID'] = $userID;
        $userData['User_Password'] = bcrypt($request->password);
        $userData['User_Parent'] = $sponserInfo->User_ID;
        $userData['User_Tree'] = $sponserInfo->User_Tree.",".$userID;
        $userData['User_RegisteredDatetime'] = date('Y-m-d H:m:s');
        $userData['User_EmailActive'] = 0;
        $userData['User_Level'] = 1;
        $userData['User_Status'] = 1;

        $insertStatus = User::insert($userData);

        if (!$insertStatus) {
            return responseRedirect(0, ERROR_CONTACT_AMDIN);
        }
        writeLog($userID, 'Register', null, 'Register User ID: '.$userID);
        $dataToken = array('user_id' => $userID, 'time' => time());
        $token = encrypt(json_encode($dataToken));

        $data = array('User_Email' => $request->email, 'token' => urlencode($token));
        sendMail('Mail.Mail-Active', $data, $request->email, 'Activate account BlueseaFunds');

        return responseRedirect(1, 'Register Success! Please check your email to active account', 'getLogin');
    }
    public function getActive(Request $request)
    {
        if ($request->token) {

            $token = ($request->token);

            $data = json_decode(decrypt(urldecode($token)));

            if (strtotime('+30 minutes', $data->time) < time()) {
                return responseRedirect(0, 'This mail expired! Please contact admin', 'getLogin');
            }
            $user = User::where('User_ID', $data->user_id)->where('User_Status', 1)->first();

            if ($user) {

                $user->User_EmailActive == 0? User::where('User_ID', $user->User_ID)->update(['User_EmailActive'=>1]):0;

                writeLog($user->User_ID, 'Active account', null, 'Active User ID: '.$user->User_ID);

                Session::put('user',$user);

                return responseRedirect(1, 'Account actice successfull', 'system.getDashboard');
            }
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN, 'getLogin');

    }
    public function RandonIDUser()
    {
        $id = rand(100000, 999999);
        //TẠO RA ID RANĐOM
        $user = User::where('User_ID', $id)->first();
        //KIỂM TRA ID RANDOM ĐÃ CÓ TRONG USER CHƯA
        if (!$user) {
            return $id;
        }
        return $this->RandonIDUser();

    }
}
