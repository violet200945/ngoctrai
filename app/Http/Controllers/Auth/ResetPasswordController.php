<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => 'bail|required|max:255',
            'new_password' => 'bail|required|max:255',
            're_new_password' => 'bail|same:new_password'
        ]);

        $user = session('user');

        $currentPassword = User::where('User_ID', $user->User_ID)->value('User_Password');

        if (Hash::check($request->current_password, $currentPassword)) {

            User::where('User_ID', $user->User_ID)->update(['User_Password' => bcrypt($request->new_password)]);

            writeLog($user->User_ID, 'Change Password');
            return responseRedirect(1, 'Password changed successful');
        }

        return responseRedirect(0,'Current password incorrect');
    }
}
