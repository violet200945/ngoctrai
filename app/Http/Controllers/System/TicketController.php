<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{
    public function getTicket()
    {
        return view('System.Ticket.ticket');
    }
}
