<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvestmentController extends Controller
{
    public function getInvestment()
    {
        return view('System.Investment.Investment');
    }
}
