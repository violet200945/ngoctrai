<?php

namespace App\Http\Controllers\System;

use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\GoogleAuth;
use Illuminate\Support\Facades\Session;
use PragmaRX\Google2FA\Google2FA;
use App\Model\Profile;
use Illuminate\Support\Facades\Storage;
use App\Model\Address;
class UserController extends Controller
{
    public function getProfile()
    {

        $user = session('user');
        $trustWalletAddress = DB::table('address')
            ->where('Address_User', $user->User_ID)
            ->where('Address_Currency', 1)
            ->value('Address_Address');

        $google2fa = app('pragmarx.google2fa');
        //kiểm tra member có secret chưa?
        $auth = GoogleAuth::where('google2fa_User',$user->User_ID)->first();

        $Enable = false;

        if (!$auth) {
            $secret = $google2fa->generateSecretKey();
            Session::put('Auth', $secret);
        }
        else {
            $Enable = true;
            $secret = $auth->User_Auth;
        }
//        $google2fa->setAllowInsecureCallToGoogleApis(true);

        $inlineUrl = $google2fa->getQRCodeUrl(
            "BlueSeaFunds",
            $user->User_Email,
            $secret
        );
        $userData['email'] = $user->User_Email;
        $userData['wallet'] = DB::table('address')->where('Address_User', $user->User_ID)
            ->select( 'Address_Currency','Address_Address')
            ->get()->toArray();
        $userData['google2fa'] = GoogleAuth::where('google2fa_User', $user->User_ID)->value('google2fa_Secret');
        $kycProfile = Profile::where('Profile_User', $user->User_ID)->first();
//        return view('System.Members.Profile', compact('user', 'trustWalletAddress','inlineUrl', 'secret', 'Enable', 'kycProfile'));
        return view('System.Users.Profile', compact('userData','inlineUrl', 'secret', 'Enable', 'kycProfile'));
    }

    public function postWalletAddress(Request $request)
    {
        $userID = session('user')->User_ID;
        if ($request->btc_wallet){
            if (!Address::where('Address_User','!=' ,$userID)->where('Address_Currency', 1)->where('Address_Address', $request->btc_wallet)->first()) {
                $btcWallet = ['Address_User' => $userID,'Address_Currency' => 1, "Address_Address" => $request->btc_wallet,'Address_isUse'=>1, 'Address_Comment' => 'Create new Bitcoin address'];
                Address::where('Address_User', $userID)->where('Address_Currency', 1)->firstOrNew($btcWallet);
            }
            else {
                return responseRedirect(0, 'BTC Wallet address early exists!');
            }


        }
        if ($request->eth_wallet){
            $ethWallet = ['Address_User' => $userID,'Address_Currency' => 2, "Address_Address" => $request->eth_wallet,'Address_isUse'=>1, 'Address_Comment' => 'Create new  address'];
            if (!Address::where('Address_User','!=' ,$userID)->where('Address_Currency', 2)->where('Address_Address', $request->eth_wallet)->first()) {
                Address::where('Address_User', $userID)->where('Address_Currency', 2)->firstOrUpdate($ethWallet);
                writeLog($userID,'Update ETH Wallet', null, null);
            }
            else {
                return responseRedirect(0, 'ETH Wallet address early exists!');
            }
        }
        return responseRedirect(1, 'Updated Wallet successful!');
    }

    public function postAuth(Request $request){
        $request->validate([
            'verifyCode' => 'required'
        ], [
            'verifyCode.required' =>'Incorrect google Authenticator ode!'
        ]);

        $user = Session('user');
        $google2fa = app('pragmarx.google2fa');
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();
        $authCode = null;
        if (Session('Auth')){
            $authCode =  session('Auth');
        }
        else {
            $authCode = $AuthUser->google2fa_Secret;
        }
        $valid = $google2fa->verifyKey($authCode, $request->verifyCode);

        if($valid){
            //kiểm tra member có secret chưa?
            $auth = GoogleAuth::where('google2fa_User',$user->User_ID)->first();

            if($auth){
                // xoá
                GoogleAuth::where('google2fa_User',$user->User_ID)->delete();
                return responseRedirect(1, 'Disable Authenticator');
            }
            else {
                // Insert bảng google2fa
                $r = new GoogleAuth();
                $r->google2fa_User = $user->User_ID;
                $r->google2fa_Secret = Session('Auth');
                $r->save();
                return responseRedirect(1,'Enable Authenticator');
            }

        }
        else {
            return responseRedirect(0,'Wrong code');
        }
    }

    public function postKYC(Request $request)
    {
        $request->validate([
            'passport' => 'required|unique:profile,Profile_Passport_ID',
            'passport_image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif|max:20480',
            'passport_image_selfie' =>'required|mimes:jpeg,jpg,bmp,png,gif|image|max:20480'
        ]);

        $user = session('user');
        $checkExist = Profile::where('Profile_User', $user->User_ID)->where('Profile_Status', [0,1] )->first();
        if ($checkExist) {
            return responseRedirect(0,"Profile early validate!");

        }
        $passportID = $request->passport;
        //get file extension
        $passportImageExtension = $request->file('passport_image')->getClientOriginalExtension();
        $passportImageSelfieExtension = $request->file('passport_image_selfie')->getClientOriginalExtension();
        //set folder and file name
        $randomNumber = uniqid();
        $passportImageStore = "users/".$user->User_ID."/profile/passport_image_".$user->User_ID."_".$randomNumber.".".$passportImageExtension;
        $passportImageSelfieStore = "users/".$user->User_ID."/profile/passport_image_selfie_".$user->User_ID."_".$randomNumber.".".$passportImageSelfieExtension;
        //send to Image server
        $passportImageStatus =Storage::disk('ftp')->put($passportImageStore, fopen($request->file('passport_image'), 'r+'));
        $passportImageSelfieStatus =Storage::disk('ftp')->put($passportImageSelfieStore, fopen($request->file('passport_image_selfie'), 'r+'));

        if ($passportImageStatus and $passportImageSelfieStatus) {
            $insertProfileData = [
                'Profile_User' => $user->User_ID,
                'Profile_Passport_ID' => $passportID,
                'Profile_Passport_Image' => $passportImageStore,
                'Profile_Passport_Image_Selfie' => $passportImageSelfieStore,
                'Profile_Time' => date('Y-m-d H:i:s')
            ];
            $inserStatus = Profile::create($insertProfileData);
            if ($inserStatus) {
                return responseRedirect(1, "Update proflie Noted");
            }
            return responseRedirect(0, ERROR_CONTACT_AMDIN);

        }

        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => "Update profile error"]);

    }

    public function getMember()
    {
        return view('System.Members.Members-List');
    }

    public function getTree()
    {
        return view('System.Members.Members-Tree');
    }

    public function getUsers() {
        return view('System.Admin.Users');
    }
}
