<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SupportController extends Controller
{
    public function logEmail()
    {
        return view('System.Admin.Support.Active-Email');
    }

    public function activeEmail()
    {
        return view('System.Admin.Support.Log-Email');
    }

    public function offAuthy()
    {
        return view('System.Admin.Support.Off-Authy');
    }
}
