<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;


class Money extends Model
{
	protected $table = "money";
    
    protected $fillable = ['Money_ID', 'Money_Game', 'Money_User', 'Money_BetAction', 'Money_USDT', 'Money_USDT_Return', 'Money_USDTFee', 'Money_Time', 'Money_Comment', 'Money_MoneyAction', 'Money_MoneyStatus', 'Money_BinaryWeak', 'Money_Package', 'Money_TXID', 'Money_Address', 'Money_Currency', 'Money_Rate', 'Money_Confirm','Money_Active'];
    
    public $timestamps = false;
    
    protected $primaryKey = 'Money_ID';
    
    public static function getBalance($user){
	    $result = DB::table('money')
	    			->where('Money_MoneyStatus', 1)
	    			->where('Money_User', $user)
	    			->selectRaw('
	    						COALESCE(SUM(IF(`Money_Currency` = 1, `Money_USDT`-`Money_USDTFee`, 0)), 0) AS BTC,
	    						COALESCE(SUM(IF(`Money_Currency` = 2, `Money_USDT`-`Money_USDTFee`, 0)), 0) AS ETH,
	    						COALESCE(SUM(IF(`Money_Currency` = 5, `Money_USDT`-`Money_USDTFee`, 0)), 0) AS USD,
	    						COALESCE(SUM(IF(`Money_Currency` = 8, `Money_USDT`-`Money_USDTFee`, 0)), 0) AS S4FX
	    			')->get();
		return $result[0];
    }
    
    public static function getBalanceCoin($user, $coin = 1){
	    $result = DB::table('money')
	    			->where('Money_MoneyStatus', 1)
	    			->where('Money_User', $user)
	    			->where('Money_Currency', $coin)
	    			->selectRaw('COALESCE(SUM(`Money_USDT`-`Money_USDTFee`), 0) AS total')->get();
		
		return $result[0]->total;
    }
    
    public static function getAddress($user){
	    $result = DB::table('address')->select('Address_Address', 'Address_Currency', 'Currency_Name', 'Currency_Symbol')
	    			->join('currency', 'Currency_ID', 'Address_Currency')
	    			->where('Address_IsUse', 0)
	    			->where('Address_User', $user)->get();
		return $result;
	}
	
	
	
	public static function insertRow($user, $mount, $comment, $currency, $action, $profits = 0, $rate = 0){
		$row = new Money();
		
		$row->Money_User = $user;
		$row->Money_USDT = $mount;
		$row->Money_Time = time();
		$row->Money_Comment = $comment;
		$row->Money_MoneyAction = $action;
		$row->Money_MoneyStatus = 1;
		$row->Money_Currency = $currency;
		$row->Money_Investment = $profits;
		$row->Money_Rate = $rate;
		if($row->save()){
			return true;
		}
		return false; 
	}
	
	public static function getHistory($user){
		$result = DB::table('money')
					->join('moneyaction', 'Money_MoneyAction', 'MoneyAction_ID')
					->join('currency', 'Money_Currency', 'Currency_ID')
					->select('Money_USDT', 'Money_USDTFee', 'Money_Time', 'Money_MoneyAction', 'Money_Currency', 'Currency_Symbol', 'MoneyAction_Name', 'Money_MoneyStatus', 'Money_Comment')
	    			->where('Money_MoneyStatus', 1)
	    			->where('Money_User', $user)->orderBy('Money_ID', 'DESC')->get();
		return $result; 
	}
	
    public static function getStatistic($where){

	    $result = Money::join('users', 'Money_User', 'User_ID')
	    				->selectRaw('`Money_User`, 
						SUM(IF(`Money_Currency` = 8 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceUPBT,
						SUM(IF(`Money_Currency` = 1 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceBTC,
						SUM(IF(`Money_Currency` = 2 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceETH,
						SUM(IF(`Money_Currency` = 5 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceUSD,
						SUM(IF(`Money_Currency` = 9 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceTRX,
						SUM(IF(`Money_Currency` = 6 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceBCH,
						SUM(IF(`Money_Currency` = 7 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceLTC,
						SUM(IF(`Money_Currency` = 10 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositUPBT, 
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositBTC, 
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositETH,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositBCH, 
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositXRP,
						
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawETH,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawXRP,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawUPBT,
									
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawETH,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawTRX,			
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawXRP,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawUPBT,
						
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveBTC,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveETH,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveUSD,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferUSD,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveUPBT,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferUPBT,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveTRX,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveBCH,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveLTC,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveXRP,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeUPBT,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeTRX,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeUSD,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentUPBT,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentUSD,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelUPBT,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelUSD,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceUPBT,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceUSD,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 5 '.$where.', `Money_USDT`, 0)) as Profit,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 13 '.$where.', `Money_USDT`, 0)) as ProfitDeposit,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 4 '.$where.', `Money_USDT`, 0)) as Direct,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 6 '.$where.', `Money_USDT`, 0)) as Indirect,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 7 '.$where.', `Money_USDT`, 0)) as Affiliate')
	    			->where('Money_MoneyStatus',1)
	    			->groupBy('Money_User');
		return $result;
    }
	
	static function StatisticTotal($where){
		$result = Money::join('users', 'Money_User', 'User_ID')->selectRaw('
						SUM(IF(`Money_Currency` = 8 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceUPBT,
						SUM(IF(`Money_Currency` = 1 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceBTC,
						SUM(IF(`Money_Currency` = 2 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceETH,
						SUM(IF(`Money_Currency` = 5 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceUSD,
						SUM(IF(`Money_Currency` = 9 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceTRX,
						SUM(IF(`Money_Currency` = 6 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceBCH,
						SUM(IF(`Money_Currency` = 7 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceLTC,
						SUM(IF(`Money_Currency` = 10 '.$where.', (ROUND((`Money_USDT` - `Money_USDTFee`),8)), 0)) as BalanceXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositUPBT, 
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositBTC, 
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositETH,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositBCH, 
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 1 '.$where.', ROUND(`Money_USDT`,8), 0)) as DepositXRP,
						
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawETH,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawXRP,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 2 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as WithDrawUPBT,
									
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawETH,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawTRX,			
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawXRP,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 15 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelWithDrawUPBT,
						
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveBTC,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveETH,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveUSD,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferUSD,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveUPBT,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferUPBT,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveTRX,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveBCH,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveLTC,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Give%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as GiveXRP,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 9 AND `Money_Comment` LIKE "Transfer%" '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as TransferXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeUPBT,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeTRX,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeUSD,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 14 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as ExchangeXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentUPBT,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentUSD,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 3 '.$where.', `Money_USDT`, 0)) as InvestmentXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelUPBT,
						SUM(IF(`Money_Currency` = 1 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelBTC,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelUSD,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 8 '.$where.', (`Money_USDT`-`Money_USDTFee`), 0)) as CancelXRP,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceUPBT,
						SUM(IF(`Money_Currency` = 2 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceETH,
						SUM(IF(`Money_Currency` = 5 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceUSD,
						SUM(IF(`Money_Currency` = 9 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceTRX,
						SUM(IF(`Money_Currency` = 6 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceBCH,
						SUM(IF(`Money_Currency` = 7 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceLTC,
						SUM(IF(`Money_Currency` = 10 AND `Money_MoneyAction` = 12 '.$where.', `Money_USDT`, 0)) as InssuranceXRP,
									
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 5 '.$where.', `Money_USDT`, 0)) as Profit,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 13 '.$where.', `Money_USDT`, 0)) as ProfitDeposit,
						
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 4 '.$where.', `Money_USDT`, 0)) as Direct,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 6 '.$where.', `Money_USDT`, 0)) as Indirect,
						SUM(IF(`Money_Currency` = 8 AND `Money_MoneyAction` = 7 '.$where.', `Money_USDT`, 0)) as Affiliate')
		    			->whereNotIn('User_Level', [1,2,3,4])
// 		    			->whereNotIn('User_ID', [393974])
						->where('User_Status', 1)
						->where('Money_MoneyStatus',1);
		return $result;
	}
	
	static function getCheckConfirm($id){
		$result = DB::table('money')
						->select('Money_ID', 'Money_User', 'Money_USDT', 'Money_USDTFee', 'Money_Currency', 'Money_Address','Money_Confirm','Money_MoneyStatus')
						->where('Money_MoneyAction',2)
						->where('Money_Confirm',0)
						->where('Money_ID',$id)->first();
		return $result;
	}
}
