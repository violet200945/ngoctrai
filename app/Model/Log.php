<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Log extends Model{
    protected $table = "log_send";

    protected $fillable = ['Log_ID','Log_Action', 'Log_Amount', 'Log_Comment', 'Log_CreatedAt', 'Log_UpdatedAt', 'Log_Status'];

    public $timestamps = true;

    const CREATED_AT = 'Log_CreatedAt';
	const UPDATED_AT = 'Log_UpdatedAt';
}
