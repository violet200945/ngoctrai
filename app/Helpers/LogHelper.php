<?php


use App\Model\Log;

if (!function_exists('writeLog')) {
    function writeLog($user, $action, $amount = null, $comment = null)
    {
        $result = new Log;
        $result->Log_User = $user;
        $result->Log_Action = $action;
        $result->Log_Amount = $amount;
        $result->Log_Comment = $comment;
        $result->Log_Status = 1;
        $result->save();
    }

}
