<?php

use GuzzleHttp\Client;

if (!function_exists('postAPI')) {
    function postAPI($url, $formData = null)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, [
            'form_params' => $formData
        ]);
        return $response;
    }
}
