<?php

if (!function_exists('sendMail')) {
    function sendMail($mailTemplate, $data, $emailAddress, $subject)
    {
        Mail::send($mailTemplate, $data, function ($msg) use ($emailAddress, $subject) {
            $msg->from('no-reply@blueseafunds.com', 'BlueSeaFunds');
            $msg->to($emailAddress)->subject($subject);
        });
    }

}
