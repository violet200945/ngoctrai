
<?php
define('ERROR_CONTACT_AMDIN' ,  'Lỗi, Vui lòng thông báo quản trị viên!');
define('SPONSER_DEFAULT', 333333);
define('CURRENCY', [
    'BTC' => 1,
    'ETH' => 2,
    'ADC' => 3,
    'USDA' => 4,
    'USD' => 5,
    'BCH' => 6,
    'LTC' => 7,
    'S4FX' => 8,
    'TRX' => 9,
    'BLU' => 10
]);
