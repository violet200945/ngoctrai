@extends('System.Layouts.Master')
@section('css')
<!-- Bootstrap Dropzone CSS -->
<link href="vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Dropzone CSS -->
<link href="vendors/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css" />

@endsection
@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Dashboar</a></li>
            <li class="breadcrumb-item active" aria-current="page">User</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i data-feather="upload"></i></span></span>USER INFORMATION</h4>
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <div class="hk-row">
                    <div class="col-lg-4">
                        <div class="card card-refresh">
                            <div class="refresh-container">
                                <div class="loader-pendulums"></div>
                            </div>
                            <div class="card-header card-header-action">

                            </div>
                            <div class="card-body">
                                <div class="hk-legend-wrap mb-40 mt-35">
                                    <div class="hk-legend">
                                        <img src="dist/img/user.jpg" alt="">
                                    </div>
                                    <button type="button" class="btn btn-primary w-80 mt-10" data-toggle="modal" data-target="#change-password">
                                        change password
                                    </button>
                                    <button type="button" class="btn btn-primary mt-20 w-80" data-toggle="modal" data-target="#change-authy">
                                        {{ ($Enable) ? 'Disable Authenticator' : 'Enable Authenticator' }}
                                    </button>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-header card-header-action">
                                <h6>USER INFORMATION</h6>

                            </div>
                            <div class="card-body pl-20">
                                <div class="row">
                                    <div class="col-sm">
                                        <form method="post" action="{{route('system.postWalletAddress')}}">
                                            @csrf
                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputEmail_1">Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-envelope-open"></i></span>
                                                    </div>
                                                    <input type="Email" class="form-control" id="email" value="Email" readonly>
                                                </div>
                                            </div>
{{--                                            {{dd($userData)}}--}}

                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputpwd_1">Bitcoin Wallet To Withdrawal</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-lock"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="btc_wallet" value="Bitcoin wallet" id="btc_wallet">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputpwd_1">Ethereum Wallet To Withdrawal</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="icon-lock"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="eth_wallet" value="Ethereum wallet" id="eth_wallet">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10" for="exampleInputpwd_2">Google Authenticator</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-primary btn-sm"><i class="icon-lock"></i></button>
                                                        <input type="text" class="form-control" value="Google Authenticator Code" id="google_auth_code" placeholder="authen" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary">Change Wallet</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <section class="hk-sec-wrapper">
                    <div class="hk-sec-title">
                        <h5>VERIFICATION</h5>
                    </div>
                    <form method="post" action="{{route('system.postKYC')}} " enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_1">ID/Passport</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-user"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="passport" id="passport" placeholder="ID/Passport">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm">
                                <label class="control-label mb-10 text-left">ID/Passport </label>
                                <p> Make sure the image is full and clear and the format is jpg, jpeg.</p>
                                <input type="file" name="passport_image" id="passport-image" class="dropify" data-default-file="dist/img/profile/1.png" accept="image/*" />
                            </div>
                            <div class="col-sm">
                                <div class="form-group mb-30">
                                    <label class="control-label mb-10 text-left">Selfie</label>
                                    <p>image is full and clear and the format is jpg, jpeg.</p>
                                    <p>Include:</p>
                                    <p><i class="fa fa-caret-right" aria-hidden="true"></i> Your face</p>
                                    <p><i class="fa fa-caret-right" aria-hidden="true"></i> Your ID/Passport</p>
                                    <div class="panel panel-default card-view">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                <div class="dropify-wrapper has-preview">
{{--                                                    <div class="dropify-message"><span class="file-icon"></span>--}}
{{--                                                        <p>Drag and drop a file here or click</p>--}}
{{--                                                        <p class="dropify-error">Ooops, something wrong appended.</p>--}}
{{--                                                    </div>--}}
                                                    <div class="dropify-loader" style="display: none;"></div>
                                                    <div class="dropify-errors-container">
                                                        <ul></ul>
                                                    </div>
{{--                                                    <input type="file" name="passport_image_selfie" id="passport_image_selfie" class="dropify" data-default-file="dist/img/profile/2.png" accept="image/*"/>--}}
                                                    <div class="dropify-preview" style="display: block;"><span class="dropify-render"><img src="img/profile/2.png"></span>
                                                        <div class="dropify-infos">
                                                            <div class="dropify-infos-inner">
                                                                <p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner">2.png</span></p>
                                                                <p class="dropify-infos-message">Drag and drop or click to replace</p>
{{--                                                                <input type="file" name="passport_image" id="passport-image" class="dropify" data-default-file="dist/img/profile/1.png" accept="image/*" />--}}

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="file" name="passport_image_selfie" id="passport_image_selfie" class="dropify" data-default-file="dist/img/profile/2.png" accept="image/*"/>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-lg">Submit</button>
                    </div>
                    </form>

                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

    <!-- Footer -->
    <!-- <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div> -->
    <!-- /Footer -->

</div>
<div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalForms" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="post" action="{{route('system.changePassword')}}">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input type="password" name="current_password" class="form-control" id="exampleDropdownFormEmail1" placeholder="Currrent password" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="new_password" class="form-control" id="exampleDropdownFormPassword1" placeholder="New Password" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="re_new_password" class="form-control" id="exampleDropdownFormPassword1" placeholder="Re new password" required>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button type="submit" class="btn btn-primary" >Cancel</button>
                        <button type="submit" class="btn btn-primary" style="margin-right: 5px!important;">Change</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<div class="modal fade" id="change-authy" tabindex="-1" role="dialog" aria-labelledby="exampleModalForms" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{($Enable) ? 'Disable Authenticator' : 'Enable Authenticator' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" action="{{route('system.postAuth')}}" method="POST" style="color:black!important;">
                            {{csrf_field()}}
                            @if(!$Enable)
                                <div style="text-align: center">
                                    Authenticator Secret Code: <b>{{ $secret }}</b><br>

                                    <img class="mt-5" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={{ $inlineUrl }}&choe=UTF-8">
                                </div>
                            @endif
                            <input type="text" name="verifyCode" class="form-control" id="exampleInputuname_01" placeholder="Enter your app code" value="">
                        <div class="input-group mr-5 justify-content-end" style="margin-top: 5px">
                            <button type="submit" class="btn btn-primary mr-5" >{{ ($Enable) ? 'Disable' : 'Enable' }}</button>
                            <button class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- Fancy Dropdown JS -->
<script src="dist/js/dropdown-bootstrap-extended.js"></script>

<!-- Dropzone JavaScript -->
<script src="vendors/dropzone/dist/dropzone.js"></script>

<!-- Dropify JavaScript -->
<script src="vendors/dropify/dist/js/dropify.min.js"></script>

<!-- Form Flie Upload Data JavaScript -->
<script src="dist/js/form-file-upload-data.js"></script>
<script>
    data = @json($kycProfile);
    if (data) {
        $('#passport').val(data.Profile_Passport_ID);
        $('#passport-image').attr('data-default-file', 'https://media.blueseafunds.com/' + data.Profile_Passport_Image);
        $("#passport_image_selfie").attr('data-default-file', 'https://media.blueseafunds.com/' + data.Profile_Passport_Image_Selfie);
    }

    userData =  @json($userData);
    $('#google_auth_code').val(userData.google2fa);
    $('#email').val(userData.email);
    $.each(userData.wallet, function (index, item) {
        if(item.Address_Currency == 1) {
            $('#btc_wallet').val(item.Address_Address)
        }
        if(item.Address_Currency == 2) {
            $('#eth_wallet').val(item.Address_Address)
        }
    });
</script>
@endsection
