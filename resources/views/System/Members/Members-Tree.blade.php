@extends('System.Layouts.Master')

@section('content')
    <div class="hk-pg-wrapper">
        <!-- Breadcrumb -->
        <nav class="hk-breadcrumb" aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-light bg-transparent">
                <li class="breadcrumb-item"><a href="#">System</a></li>
                <li class="breadcrumb-item"><a href="#">Members</a></li>
                <li class="breadcrumb-item active" aria-current="page">Members Tree</li>
            </ol>
        </nav>
        <!-- /Breadcrumb -->

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">
            <section class="hk-sec-wrapper col-md-5 ur-link">
                <h5 class="hk-sec-title">Ur Link Present</h5>
                <div class="row">
                    <div class="col-sm">
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-6 mb-10">
                                    <input type="text" class="form-control" id="validationCustom03" value="Ur Link" readonly>
                                </div>
                                <div class="col-md-3 mb-10">
                                    <button class="btn btn-primary" type="submit">Copy</button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </section>
            <section class="hk-sec-wrapper col-md-5 ur-link">
                <h5 class="hk-sec-title">Present New Member</h5>
                <div class="row">
                    <div class="col-sm">
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                                <div class="col-md-6 mb-10">
                                    <input type="text" class="form-control" id="validationCustom03" placeholder="City" required>
                                </div>
                                <div class="col-md-6 mb-10">
                                    <button class="btn btn-primary" type="submit">Confirm</button>
                                    <button class="btn btn-primary" type="submit">Reset</button>
                                </div>


                            </div>

                        </form>
                    </div>
                </div>
            </section>
        </div>
        <!-- /Row -->
        <section class="hk-sec-wrapper">
            <h5 class="hk-sec-title">Member Tree</h5>
          
        </section>
            <!-- /Row -->

        </div>
        <!-- /Container -->

        <!-- Footer -->
        <div class="hk-footer-wrap container">
            <footer class="footer">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p class="d-inline-block">Follow us</p>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /Footer -->

    </div>
@endsection
