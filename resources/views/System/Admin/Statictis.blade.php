@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Statictis</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">

                    <div class="row">
                        <div class="col-sm">
                            <form>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control" id="email" placeholder="" value="" type="email">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="user">User</label>
                                        <input class="form-control" id="user" placeholder="" value="" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="datefrom">Date from</label>
                                        <input class="form-control" id="datefrom" placeholder="" value="" type="date">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="dateto">Date to</label>
                                        <input class="form-control" id="dato" placeholder="" value="" type="date">
                                    </div>
                                </div>

                                <hr>
                                <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i>Search</button>
                            </form>
                        </div>
                    </div>
                </section>
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">List User</h5>
                  <button class="btn btn-primary" type="submit">Exel</button>
                    <label class="ml-30">
                        search:
                        <input type="search">
                       
                    </label>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap mb-20">
                                <div class="table-responsive">
                                    <table class="table table-blue table-bordered mb-0">
                                        <thead class="thead-blue">
                                            <tr>
                                                <th>User Id</th>
                                                <th>User Level</th>
                                                <th>Deposit</th>
                                                <th>Balance</th>
                                                <th>Interest</th>
                                                <th>commission</th>
                                                <th>investiment</th>
                                                <th>withdraw</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Jens</td>
                                                <td>Brincker</td>
                                                <td>Brincker123</td>
                                                <td><span class="badge badge-danger">admin</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Mark</td>
                                                <td>Hay</td>
                                                <td>Hay123</td>
                                                <td><span class="badge badge-info">member</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Anthony</td>
                                                <td>Davie</td>
                                                <td>Davie123</td>
                                                <td><span class="badge badge-warning">developer</span> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection