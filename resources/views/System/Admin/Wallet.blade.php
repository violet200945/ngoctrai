@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Members List</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                   
                    <div class="row">
                        <div class="col-sm">
                            <form>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="UserID">UserID</label>
                                        <input class="form-control" id="UserID" placeholder="" value="" type="text">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="User">User</label>
                                        <input class="form-control" id="User" placeholder="" value="" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="userTree">User Tree</label>
                                        <input class="form-control" id="userTree" placeholder="" value="" type="text">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="Action">Action</label>
                                        <select class="form-control" tabindex="1" name="action">
                                            <option value="">---selected---</option>
                                            <option value="1">Deposit</option>
                                            <option value="2">Withdraw</option>
                                            <option value="3">Investment</option>
                                            <option value="4">Interest</option>
                                            <option value="5" v="">Direct Commisstion</option>
                                            <option value="6">Affiliate Commission</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="datefrom">Date from</label>
                                        <input class="form-control" id="datefrom" placeholder="" value="" type="date">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="dateto">Date to</label>
                                        <input class="form-control" id="dato" placeholder="" value="" type="date">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="Action">Status</label>
                                        <select class="form-control" tabindex="1" name="action">
                                            <option value="">---selected---</option>
                                            <option value="">confirm</option>
                                            <option value="">pending</option>
                                            <option value="">erro</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <button class="btn btn-primary" type="submit"><i class="fas fa-search">Search</i></button>
                                        <button class="btn btn-warning" type="submit"><i class="fas fa-file-excel">Export</i></button>
                                    </div>
                                </div>



                            </form>
                        </div>
                    </div>
                </section>
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title"> List wallet Table</h5>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap mb-20">
                                <div class="table-responsive">
                                    <table class="table table-blue table-bordered mb-0">
                                        <thead class="thead-blue">
                                        <tr>
                                            <th>ID</th>
                                            <th>Level</th>
                                            <th>User ID</th>
                                            <th>Amount</th>
                                            <th>Amount USD</th>
                                            <th>Fee</th>
                                            <th>Fee USD</th>
                                            <th>Rate</th>
                                            <th>Currency</th>
                                            <th>Action</th>
                                            <th>Time</th>
                                            <th>Comfirm</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Jens</td>
                                                <td>Brincker</td>
                                                <td>Brincker123</td>
                                                <td><span class="badge badge-danger">admin</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Mark</td>
                                                <td>Hay</td>
                                                <td>Hay123</td>
                                                <td><span class="badge badge-info">member</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Anthony</td>
                                                <td>Davie</td>
                                                <td>Davie123</td>
                                                <td><span class="badge badge-warning">developer</span> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection