@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ticket</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <section class="hk-sec-wrapper col-md-3 tick-sup">
                    <h5 class="hk-sec-title">Support Ticket</h5>
                    <div class="row">
                        <div class="col-sm">
                            <form>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Subject </label>
                                    <select type="text" class="form-control" name="tickect_subject">
                                        <option value="1">Request</option>
                                        <option value="2">Send</option>
                                        <option value="3">Investment</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                        <label class="control-label mb-10 text-left">Description</label>
                                        <textarea class="form-control" name="ticket_content" placeholder="Description your problems..." rows="5"></textarea>
                                    </div>
                                <hr>
                                <button class="btn btn-primary" type="submit">Send</button>
                            </form>
                        </div>
                    </div>
                </section>
                <section class="hk-sec-wrapper col-md-8 tick-sup">
                    <h5 class="hk-sec-title">Wallet Deposit on Day</h5>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap mb-20">
                                <div class="table-responsive">
                                    <table class="table table-success table-bordered mb-0">
                                        <thead class="thead-blue">
                                            <tr>
                                                <th>Serial</th>
                                                <th>date</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>12/12/2019</td>
                                                <td>Jens</td>
                                                <td>Brincker</td>
                                                <td>Brincker123</td>
                                                <td><span class="badge badge-danger">Fail</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>12/12/2019</td>
                                                <td>Jens</td>
                                                <td>Brincker</td>
                                                <td>Brincker123</td>
                                                <td><span class="badge badge-success">Succes</span> </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection