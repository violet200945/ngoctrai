@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item active" aria-current="page">Wallet</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Deposit Form</h5>
                    <div class="row">
                        <div class="col-sm">
                            <form>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="firstName">First name</label>
                                        <input class="form-control" id="firstName" placeholder="" value="" type="text">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="lastName">Last name</label>
                                        <input class="form-control" id="lastName" placeholder="" value="" type="text">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5 mb-10">
                                        <label for="country">Country</label>
                                        <select class="form-control custom-select d-block w-100" id="country">
                                            <option value="">Choose...</option>
                                            <option>United States</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 mb-10">
                                        <label for="state">State</label>
                                        <select class="form-control custom-select d-block w-100" id="state">
                                            <option value="">Choose...</option>
                                            <option>California</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="zip">Zip</label>
                                        <input class="form-control" id="zip" placeholder="" type="text">
                                    </div>
                                </div>
                                <hr>

                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" id="save-info" type="checkbox">
                                    <label class="custom-control-label" for="save-info">Save this information for next time</label>
                                </div>
                                <hr>

                                <h6 class="form-group">Payment</h6>

                                <div class="d-block mt-20 mb-30">
                                    <div class="custom-control custom-radio mb-10">
                                        <input id="credit" name="paymentMethod" class="custom-control-input" checked="" type="radio">
                                        <label class="custom-control-label" for="credit">Credit card</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-10">
                                        <input id="debit" name="paymentMethod" class="custom-control-input" type="radio">
                                        <label class="custom-control-label" for="debit">Debit card</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input id="paypal" name="paymentMethod" class="custom-control-input" type="radio">
                                        <label class="custom-control-label" for="paypal">PayPal</label>
                                    </div>
                                </div>

                                <hr>
                                <button class="btn btn-primary" type="submit">Continue to checkout</button>
                            </form>
                        </div>
                    </div>
                </section>
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Withdrawal Commands</h5>

                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <table id="datable_1" class="table table-hover w-100 display pb-30">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>withdrawal Amount</th>
                                            <th>exchange rate</th>
                                            <th>withdrawal fee</th>
                                            <th>revice wallet</th>
                                            <th>withdrawal time</th>
                                            <th>withdrawal status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>2011/04/25</td>
                                            <td>$320,800</td>
                                            <td>$320,800</td>
                                        </tr>



                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection