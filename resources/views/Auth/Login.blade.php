<!DOCTYPE html>
<!--
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: support@hencework.com

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="en">
	<head>
		<meta charset="UTF-8" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<title>Login</title>

        <base href="{{asset('')}}">

		<meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">

		<!-- Toggles CSS -->
		<link href="vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
		<link href="vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">

        <!-- Toastr CSS -->
        <link href="vendors/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

        <!--Sweetalert -->
        <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css" />

		<!-- Custom CSS -->
		<link href="dist/css/style.css" rel="stylesheet" type="text/css">

        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

	</head>
	<body>
		<!-- Preloader -->
		<div class="preloader-it">
			<div class="loader-pendulums"></div>
		</div>
		<!-- /Preloader -->
{{--        {{dd(Session::get())}}--}}
		<!-- HK Wrapper -->
		<div class="hk-wrapper">

			<!-- Main Content -->
			<div class="hk-pg-wrapper hk-auth-wrapper">
				<header class="d-flex justify-content-end align-items-center">

				</header>
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12 pa-0  card-body ">
							<div class="auth-form-wrap pt-xl-0 pt-70">
								<div class="auth-form w-xl-30 w-lg-55 w-sm-75 w-100">
									<a class="auth-brand text-center d-block mb-20" href="#">
										<img class="brand-img" src="dist/img/logo-light.png" alt="brand"/>
									</a>
									<form method="post" action="" id="login-form">
                                        @csrf
										<h1 class="display-4 text-center mb-10">Welcome Back :)</h1>
										<p class="text-center mb-30">Sign in to your account and enjoy.</p>
										<div class="form-group">
											<input class="form-control" placeholder="Email" type="email" name="email" required>
										</div>
										<div class="form-group">
											<div class="input-group">
												<input class="form-control" placeholder="Password" type="password" name="password" required>
											</div>
										</div>
                                        <div class="form-group align-middle" >
                                            <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}" ></div>
                                        </div>
										<button class="btn btn-primary btn-block" type="submit">Login</button>
										<a href="{{route('getForgotPassword')}}" class="font-14 text-center mt-15">Having trouble logging in?</a>
										<div class="option-sep">or</div>
										<p class="text-center">Do have an account yet? <a href="{{route('getRegister')}}">Sign Up</a></p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /Main Content -->

		</div>
		<!-- /HK Wrapper -->

		<!-- JavaScript -->

		<!-- jQuery -->
		<script src="vendors/jquery/dist/jquery.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="vendors/popper.js/dist/umd/popper.min.js"></script>
		<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>

		<!-- Slimscroll JavaScript -->
		<script src="dist/js/jquery.slimscroll.js"></script>

		<!-- Fancy Dropdown JS -->
		<script src="dist/js/dropdown-bootstrap-extended.js"></script>

		<!-- FeatherIcons JavaScript -->
		<script src="dist/js/feather.min.js"></script>

        <!-- Toastr JS -->
        <script src="vendors/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

        <!-- Sweet alert -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>

		<!-- Init JavaScript -->
		<script src="dist/js/init.js"></script>



        <script>
            $(document).ready(function() {
                $("#login-form").submit(function(event) {

                    var recaptcha = $("#g-recaptcha-response").val();
                    if (recaptcha === "") {
                        event.preventDefault();
                        $.toast({
                            heading: 'Error!',
                            text: "<p>Please enter capchar!</p>",
                            position: 'top-right',
                            loaderBg: '#009fe1',
                            class: 'jq-toast-error',
                            hideAfter: 3500,
                            stack: 6,
                            showHideTransition: 'fade'
                        });
                    }
                });
                @if(Session::has('errors'))
                    $.toast({
                        heading: 'Error!',
                        text: "<p>{{ Session::get('errors')->first() }}</p>",
                        position: 'top-right',
                        loaderBg:'#009fe1',
                        class: 'jq-toast-error',
                        hideAfter: 3500,
                        stack: 6,
                        showHideTransition: 'fade'
                    });
                @endif
                @if(Session::get('flash_level')  == 'success')
                    $.toast({
                        heading: 'Sucess!',
                        text: "<p>{{ Session::get('flash_message')}}</p>",
                        position: 'top-right',
                        loaderBg:'#009fe1',
                        class: 'jq-toast-success',
                        hideAfter: 3500,
                        stack: 6,
                        showHideTransition: 'fade'
                    });
                @endif
                @if(Session::get('flash_level')  == 'error')
                $.toast({
                    heading: 'Error!',
                    text: "<p>{{ Session::get('flash_message')}}</p>",
                    position: 'top-right',
                    loaderBg:'#009fe1',
                    class: 'jq-toast-error',
                    hideAfter: 3500,
                    stack: 6,
                    showHideTransition: 'fade'
                });
                @endif
                @if(Session::has('otp'))
                    var CSRF_TOKEN = '{{ csrf_token() }}';
                    swal.fire({
                        title: 'Enter Authentication',
                        text: 'Please enter authentication code.',
                        input: 'text',
                        type: 'input',
                        name: 'txtOTP',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showLoaderOnConfirm: true,
                        confirmButtonClass: 'btn btn-confirm',
                        cancelButtonClass: 'btn btn-cancel'
                    }).then(function (otp) {
                        console.log(otp);
                        $.ajax({
                            url: "{{route('postLoginCheckOTP')}}",
                            type: 'POST',
                            data: {_token: CSRF_TOKEN, otp:otp.value},
                            success: function (data) {
                                if(data == 1){
                                    location.href = "{{route('system.getDashboard')}}";
                                }else{
                                    swal.fire({
                                        title: 'Error',
                                        text: "Authentication Code Is Wrong",
                                        type: 'error',
                                        confirmButtonClass: 'btn btn-confirm',
                                        allowOutsideClick: false
                                    }).then(function() {
                                        location.href = "{{route('getLogin')}}";
                                    })
                                }
                            }
                        });
                    });
                @endif
            });
        </script>
	</body>
</html>
